The monitoring interface
========================

These are few random ideas about how the monitoring interface would
look like.

We could specify the part of network traffic that we want to see. It
would be either everything, or restricted to:

 * Certain local computer (or a group of)
 * Certain protocol (http-in and http-out are two different protocols)
 * Some remote site (eg. „facebook“)

Or some combination of these (eg. this computer talking to facebook).
We may decide not to allow specification of all three at once, in case
the storage requirements would be to large.

In each node we would see the current traffic (eg. current list of
flows and a sum of them). We would also see historical data,
presumably in a form of a graph (or multiple overlaid graphs, with
speed in/out, amount of data in/out, number of packets, number of
total flows at that moment). The graph would allow „zooming in“ to
more fine-grained data (eg. by minute instead of hour), but only at
certain interval of the freshest data.

The view would have several buttons, leading to configuration
interface (eg. to block similar traffic, shaping configuration,
request to place it into a PCAP).

Also, features we may want to have:

* Ability to alert when some new kind of communication happens (eg.
  when the fridge starts talking to some new servers).
* Exclamation marks at unsafe protocols/clients using unsafe
  protocols.
* Geo-IP country flags.
* Per-client „other“ category if the allowed amount of sub-categories
  is reached (eg. unlike majordomo, each separate client would have
  their own 500 remote servers and all others would be stuffed into
  this wildcard category).
* Be able to say „Hey, this is also facebook“
* Be able to share what this „facebook thing“ means to me, so others
  can reuse.
